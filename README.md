# myProfiler

basic decorator for time profiling of functions


## example

```python 

from myprofiler import myprofiler

@myprofiler
def _test(n):
    for i in range(n):
        pass
    return 100


_test(10)

```