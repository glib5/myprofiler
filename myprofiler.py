import cProfile
import pstats
from typing import Callable


def myprofiler(func:Callable):
    """decorator for standard profiling of 'func'"""
    def _profileit(*args, **kwargs):
        with cProfile.Profile() as pr:
            res = func(*args, **kwargs)
        stats = pstats.Stats(pr)
        stats.sort_stats(pstats.SortKey.TIME)
        print()
        stats.print_stats()
        # stats.dump_stats(filename=f"profile.prof")
        return res
    return _profileit
