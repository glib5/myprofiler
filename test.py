from random import random

from myprofiling import myprofiler


@myprofiler
def _test():
    for i in range(1<<20):
        x = random()
    return x


def main():    
    _test()
    return 0


if __name__=="__main__":
    main()
